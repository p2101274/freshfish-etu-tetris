#include "position.hpp"

#include <stdexcept>

Position voisine(const Position& pos, int direction) {
    Position res = pos ;

    switch(direction) {
    case 0: // haut
        res.first-- ;
        break ;
    case 1: // gauche
        res.second++ ;
        break ;
    case 2: // bas
        res.first++ ;
        break ;
    case 3: // droite
        res.second-- ;
        break ;
    default:
        throw std::invalid_argument("Direction invalide") ;
    }

    return res ;
}

std::ostream& operator<<(std::ostream& out, const Position& pos) {
    out << "{" << pos.first << "," << pos.second << "}" ;
    return out ;
}
